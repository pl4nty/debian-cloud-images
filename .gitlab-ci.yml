---
stages:
  - source test
  - build
  - vendor upload
  - upload

variables:
  CLOUD_IMAGE_BUILD_ID: ${CI_PROJECT_NAMESPACE}-${CI_COMMIT_REF_SLUG}
  GIT_DEPTH: "1"

before_script:
  - apt-get update
  - apt-get install --no-install-recommends -y python3-libcloud python3-marshmallow python3-yaml qemu-utils
  - |
    if [ "$CI_DISPOSABLE_ENVIRONMENT" ]; then
      # Workaround unsupported SEEK_HOLE in overlayfs (Docker default)
      mount -t tmpfs none /tmp
    fi

test package:
  stage: source test
  image: debian:bookworm
  script:
    - apt-get build-dep -y ./
    - >
      apt-get install --no-install-recommends -y
      lintian
    - DEB_BUILD_OPTIONS=nocheck dpkg-buildpackage -us -uc
    - lintian ../*.changes

.test python:
  stage: source test
  script:
  - >
    apt-get install --no-install-recommends -y
    python3-pytest python3-requests-mock
  - >
    PYTHONPATH=src
    py.test-3
    -vrxXs
    --color=yes
    tests

test python bullseye:
  extends: .test python
  image: debian:bullseye

test python bookworm:
  extends: .test python
  image: debian:bookworm

test python latest:
  stage: source test
  image: python:slim
  before_script:
  - pip install -e .[mypy,tests] pytest pytest-cov pytest-flake8 pytest-mypy
  script:
  - >
    pytest
    -vrxXs
    --color=yes
    --cov=src --cov-report=term --cov-report=html:coverage
    --flake8
    --mypy
    --junit-xml=junit.xml
  artifacts:
    paths:
    - coverage/
    reports:
      junit: junit.xml

####
# Builds for developer uploads pushes
#
# Builds are run if
# - the manual jobs are started.
####

.build:
  stage: build
  image: debian:bookworm
  script:
    - |
      if [ "$CI_DISPOSABLE_ENVIRONMENT" ]; then
        # Some of our build environments run with SELinux enabled, make sure it is detected by all the tools
        if [ -d /sys/fs/selinux ]; then mount -t selinuxfs none /sys/fs/selinux; mkdir -p /etc/selinux; touch /etc/selinux/config; fi
        # Setup binfmt_misc for foreign binaries via qemu-user
        if [ ! -e /proc/sys/fs/binfmt_misc/status ]; then
          mount -t binfmt_misc binfmt_misc /proc/sys/fs/binfmt_misc
        fi
      fi
    - >
      apt-get install --no-install-recommends -y
      binfmt-support ca-certificates debsums dosfstools fai-server fai-setup-storage fdisk make python3-pytest qemu-user-static qemu-utils sudo udev
    - >
      echo
      debian-cloud-images build
      --build-id ${CLOUD_IMAGE_BUILD_ID}
      --build-type dev
      --version ${CI_PIPELINE_IID}
      ${CLOUD_RELEASE} ${CLOUD_VENDOR} ${CLOUD_ARCH}
    - >
      ./bin/debian-cloud-images build
      --build-id ${CLOUD_IMAGE_BUILD_ID}
      --build-type dev
      ${CLOUD_RELEASE} ${CLOUD_VENDOR} ${CLOUD_ARCH}
    - 'xz -vk5T0 *.tar'
  artifacts:
    name: debian-${CLOUD_RELEASE}-${CLOUD_VENDOR}-${CLOUD_ARCH}
    expire_in: 7 days
    paths:
      - '*.build.json'
      - '*.info'
      - '*.tar.xz'
    reports:
      junit: '*.build.junit.xml'
  needs: []
  rules:
  - if: '$CI_MERGE_REQUEST_ID == null'
    when: manual
  allow_failure: true

####
# Provider uploads for developer pushs
####

.azure upload:
  stage: vendor upload
  image: debian:bookworm
  script:
    - >
      ./bin/debian-cloud-images upload-azure
      --config-file "${CLOUD_UPLOAD_DEV_CONFIG}"
      *.build.json
  artifacts:
    name: upload-azure
    expire_in: 7 days
    paths:
      - '*.upload-azure.json'
  rules:
  - if: '$CI_MERGE_REQUEST_ID == null && $CLOUD_UPLOAD_AZURE_DEV_ENABLED == "1"'
    when: manual
  allow_failure: true

.ec2 upload:
  stage: vendor upload
  image: debian:bookworm
  script:
    - >
      ./bin/debian-cloud-images upload-ec2
      --config-file "${CLOUD_UPLOAD_DEV_CONFIG}"
      *.build.json
  artifacts:
    name: upload-ec2
    expire_in: 7 days
    paths:
      - '*.upload-ec2.json'
  rules:
  - if: '$CI_MERGE_REQUEST_ID == null && $CLOUD_UPLOAD_EC2_DEV_ENABLED == "1"'
    when: manual
  allow_failure: true

.gce upload:
  stage: vendor upload
  image: debian:bookworm
  script:
    - >
      ./bin/debian-cloud-images upload-gce
      --config-file "${CLOUD_UPLOAD_DEV_CONFIG}"
      *.build.json
  artifacts:
    name: upload-gce
    expire_in: 7 days
    paths:
      - '*.upload-gce.json'
  rules:
  - if: '$CI_MERGE_REQUEST_ID == null && $CLOUD_UPLOAD_GCE_DEV_ENABLED == "1"'
    when: manual
  allow_failure: true

include:
  local: .gitlab/ci/generated.yml
